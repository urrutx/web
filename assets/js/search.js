
var options = {
    searchInput: document.getElementById('search-input'),
    resultsContainer: document.getElementById('search-results'),
    searchContainer: document.getElementById('search-container'),
    json: '/search.json',
    searchResultTemplate: '<li><h4 class="{tag_class}">{tag}</h4><a href="{url}">{title}</a></li>',
    noResultsText: 'No hay resultados',
    limit: 5,
    exclude: []
}

init(options);

var data=[];

function init(options) {
    load(options.json)
    registerInput()
}

//get JSON via fetch
function load (location) {
    fetch(
        location,
        { method: 'GET' }
    )
        .then( response => response.json() )
        .then( json => data=json )
        .catch( error =>  console.log("JSON fetch error"+location+error));

}


function search(crit, limit) {
    if (crit.length==0) {
        return []
    }
    else{
        return findMatches(data, crit, limit)
    }
}

function findMatches (data, crit, limit) {
    var matches = []
    for (let i = 0; i < data.length && matches.length < limit; i++) {
        console.log(crit, data[i]["title"])
        if (match(data[i]["title"], crit)) {
            matches.push(data[i])
        }
    }
    return matches
}

function match(str, crit) {
    if (!str) return false

    str = str.trim().toLowerCase()
    crit = crit.trim().toLowerCase()

    return crit.split(' ').filter(function (word) {
        return str.indexOf(word) >= 0
    }).length === crit.split(' ').length
}

//Show matches
function showMatches(query) {
    emptyResultsContainer()
    render(search(query, options.limit), query)
}

function render (results, query) {
    var len = results.length
    if (len === 0) {
        return appendToResultsContainer(options.noResultsText)
    }
    for (let i = 0; i < len; i++) {
        results[i].query = query
        appendToResultsContainer(compile(results[i]))
    }
}

function compile (data) {
    return options.searchResultTemplate.replace(/\{(.*?)\}/g, function (match, prop) {
        return data[prop] || match
    })
}

//DOM
function emptyResultsContainer () {
    options.resultsContainer.innerHTML = ''
}

function appendToResultsContainer (text) {
    options.resultsContainer.innerHTML += text
}

function registerInput () {
    let search_button=document.getElementById("search_button");
    search_button.addEventListener('click', function (e) {
        if(options.searchContainer.classList.contains("hide")){
            options.searchContainer.classList.remove("hide");
             search_button.src="/assets/images/svg/close-icon.svg"
        }
        else{
            options.searchContainer.classList.add("hide");
            search_button.src="/assets/images/svg/search-icon.svg"
        }
    })
    options.searchInput.addEventListener('keyup', function (e) {
        if (isWhitelistedKey(e.which)) {
            emptyResultsContainer()
            showMatches(e.target.value)
        }
    })
}



function isWhitelistedKey (key) {
    return [13, 16, 20, 37, 38, 39, 40, 91].indexOf(key) === -1
}


